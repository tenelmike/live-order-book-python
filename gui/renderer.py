
import asyncio
import time
import logging

from tkinter import TclError

from gui.frontend import draw_bids, draw_asks, draw_fills, draw_order_book_depth, launch_gui


# Create a logger object
logger = logging.getLogger('system')

# Set the log level to DEBUG so that all messages are logged
logger.setLevel(logging.DEBUG)


def gui_cb(renderer):
    """
    Uses the tk function after to schedule a new order using a callback. Also calls the main update functions for
    the framework's windows

    :param renderer: The OrderBookRenderer
    :return: void
    """
    renderer.main_window.update_idletasks()
    renderer.main_window.update()


class OrderBookRenderer:
    """
    Uses the tkinter GUI framework to render an OrderBook
    """

    def __init__(self):
        """
        Initializes the renderer
        """
        # if the renderer is running
        self.run = True

        # The websocket class from which we are rendering orders and trades
        # can send API calls when the user interacts with the GUI
        self.websocket = None

        # The REST API class that can send API calls when the user interacts with the GUI
        self.rest_client = None

        # Callback to be run on shutdown when the user presses the X button on the GUI
        self.shutdown_cb = None

        # initializes the gui components using the helper function
        (self.main_window, self.bid_treeview, self.ask_treeview, self.fill_treeview,
         self.fig, self.fig_canvas) = launch_gui()

    def set_api_connections(self, websocket, rest_client):
        """
        Sets the websocket and REST API client references so the GUI can interact after user actions
        """
        self.websocket = websocket
        self.rest_client = rest_client

    def set_shutdown_callback(self, callback):
        """
        Sets a callback that will be called when the user presses the X button on the GUI window

        :param callback: The callback to run
        """
        self.shutdown_cb = callback

    def terminate(self):
        """
        Function allowing the renderer to be terminated gracefully by another thread or callback
        :return: void
        """
        self.run = False

    # async def loop(self, gui_callbacks=gui_cb):
    def loop(self, gui_callbacks=gui_cb):
        """
        The main renderer loop

        :param gui_callbacks: Any callbacks the gui needs to run. For tkinter there are some
        :return: void
        """
        while self.run:

            if self.shutdown_cb is not None:
                self.main_window.protocol("WM_DELETE_WINDOW", lambda: self.shutdown_cb(self))

            try:
                # 60fps
                # await asyncio.sleep(0.016)
                time.sleep(0.016)

                if self.websocket is not None:
                    # render the table of filled orders if the gui component exists and not running headless
                    if self.fill_treeview is not None:
                        draw_fills(self.websocket.trade_fills, self.fill_treeview)

                    # render the bid table if the gui component exists and not running headless
                    if self.bid_treeview is not None:
                        draw_bids(self.websocket.order_book.bids, self.bid_treeview)

                    # render the ask table if the gui component exists and not running headless
                    if self.ask_treeview is not None:
                        draw_asks(self.websocket.order_book.asks, self.ask_treeview)

                    # render the order book depth plot if the gui component exists
                    if self.fig is not None:
                        draw_order_book_depth(self.websocket.order_book, self.fig, self.fig_canvas)

                # call the gui callbacks if specified
                if gui_callbacks is not None:
                    gui_callbacks(self)
            except TclError:
                # if there are problems (like the gui X button being pressed) gracefully shut down the renderer
                self.terminate()
            except asyncio.TimeoutError:
                # call the gui callbacks if specified
                if gui_callbacks is not None:
                    gui_callbacks(self)
        logger.info('Renderer Shutdown, Destroying GUI')
        self.main_window.destroy()
