
# Live Order Book in Python

Simple demo app that displays a live order book from the Kucoin Crypto Exchange. You will need to create 
a .env file in the root directory of the project and populate it with the following values:

```python
KUCOIN_API_SECRET='your-secret'
KUCOIN_API_KEY='your-key'
KUCOIN_API_PASSWORD='your-api-password'
```
Note: This password is the one created for the API key and not the 6-digit 'trading password'

I am running locally using python 3.9 and poetry.

Run it with the standard command for poetry:

```commandline
poetry run main.py
```

Demo output:
![A screenshot of the GUI during operation](media/gui-demo.png)
