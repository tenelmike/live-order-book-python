
import logging
import asyncio
import time
import threading

from gui.renderer import OrderBookRenderer
from kucoin.rest.client import KucoinRestClient
from kucoin.websocket.websocket import KuCoinWebsocket
from kucoin.websocket.ws_utils import message_processing_coroutine


# Create a logger object
logger = logging.getLogger('system')

# Set the log level to DEBUG so that all messages are logged
logger.setLevel(logging.DEBUG)


def system_shutdown_callback(renderer: OrderBookRenderer):
    """
    Callback that shuts down the system on some event like the user pressing the X to close the GUI

    :param renderer: The renderer displaying the GUI
    """
    logger.info('Initiating System Shutdown')

    rest_client = renderer.rest_client
    websocket = renderer.websocket

    # Gracefully terminate the websocket, rest session, and renderer
    websocket.terminate()
    rest_client.terminate()
    renderer.terminate()

    websocket.close_websocket()
    logger.info('Shutdown Callback Completed')


async def initialize_system(loop, renderer, symbol):
    """
    Initializes the system by configuring the REST API, the websocket, and the renderer

    :param loop: The asyncio event loop to schedule coroutines
    :param renderer: The renderer that displays the GUI to the user
    :param symbol: The ticker symbol we are interested in
    """

    # set the renderer to call the shutdown callback when the X is pressed on the GUI
    renderer.set_shutdown_callback(system_shutdown_callback)

    # start the REST API and get the information needed to configure the websocket
    rest_client = KucoinRestClient()

    ws_info = await rest_client.get_websocket_channel_info()
    symbol_info = await rest_client.get_symbol_info(symbol)
    ticker_info = await rest_client.get_ticker(symbol)

    logger.info(ticker_info)
    logger.info(symbol_info)

    websocket = KuCoinWebsocket(loop, ws_info, private_ws=False)

    # await asyncio.sleep(3)
    time.sleep(3)

    await websocket.subscribe(f'/market/level2:{symbol}')
    await websocket.subscribe(f'/market/match:{symbol}')

    # await asyncio.sleep(1)
    time.sleep(1)

    websocket.order_book = await rest_client.get_order_book_snapshot(
            symbol, ticker_info.price, symbol_info.price_increment)

    renderer.set_api_connections(websocket, rest_client)

    tasks = [
        asyncio.create_task(message_processing_coroutine(websocket))
    ]

    await asyncio.gather(*tasks)

    logger.info('Async coroutine processing complete')


def start_async_coroutine_loop(renderer, symbol):
    """
    Starts the event loop for scheduling coroutines in this thread and then initializes the system

    :param renderer: The renderer managing the GUI
    :param symbol: The symbol we are interested in
    """

    # create a new event loop in this thread for scheduling coroutines
    loop = asyncio.new_event_loop()
    loop.run_until_complete(initialize_system(loop, renderer, symbol))


def run_coroutine_thread(renderer, symbol):
    """
    Starts the thread that runs async coroutines so that they do not interfere with the GUI rendering event loop
    which is required to be located in the main thread

    :param renderer: The renderer managing the GUI
    :param symbol: The symbol we are interested in
    """
    _t = threading.Thread(target=start_async_coroutine_loop, args=(renderer, symbol))
    _t.start()
    return _t


def main_fcn(symbol: str):
    """
    The main function of the application

    :param symbol: The symbol we are interested in, e.g. BTC-USDT
    """
    renderer = OrderBookRenderer()

    # launch the thread for processing coroutines
    _t = run_coroutine_thread(renderer, symbol)

    # run the GUI main event loop for processing user interactions
    renderer.loop()

    logger.info('Main Thread Shutdown')
