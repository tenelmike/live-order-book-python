
import copy
import logging

import time
import aiohttp
import asyncio

from decimal import Decimal
from typing import List, Union

from order_book import OrderBook
from kucoin.utils import WebsocketInfo, SymbolInfo, TickerInfo, get_kucoin_system_error_message, get_http_error_message
from kucoin.rest.utils import (generate_get_rest_signature, generate_rest_signature, build_http_headers)


# Create a logger object
logger = logging.getLogger('kucoin.rest')

# Set the log level to DEBUG so that all messages are logged
logger.setLevel(logging.DEBUG)


class KucoinRestClient:

    REST_API_URL = 'https://openapi-v2.kucoin.com'
    SANDBOX_API_URL = 'https://openapi-sandbox.kucoin.com'

    def __init__(self, sandbox=False, requests_params=None):
        """
        Creates a REST client for Kucoin

        :param sandbox:
        :param requests_params:
        """
        if sandbox:
            self.API_URL = self.SANDBOX_API_URL
        else:
            self.API_URL = self.REST_API_URL

        self._requests_params = requests_params

        self.headers_dict = build_http_headers()
        self.session = None
        self.shutdown = False

    def terminate(self):
        _loop = asyncio.get_event_loop()
        _loop.create_task(self.close())

    async def close(self):
        logger.info('Closing REST Session')
        await self.session.close()
        self.shutdown = True
        logger.info('REST Session Closed')

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc, tb):
        await self.close()

    def __del__(self):
        logger.info('REST API Client Shutdown')

    async def get_request(self, path_ending: str, params: dict = None):

        if self.session is None:
            self.session = aiohttp.ClientSession()

        url = self.API_URL + path_ending

        # create the request headers
        request_headers = copy.deepcopy(self.headers_dict)
        request_headers['timeout'] = str(10)

        # sign the request
        # nonce = make_kucoin_unique_id_int()
        # from the API docs
        now = int(time.time() * 1000)
        request_headers['KC-API-TIMESTAMP'] = str(now)
        request_headers['KC-API-SIGN'] = generate_get_rest_signature(now, path_ending, params).decode('utf-8')

        async with self.session.get(url, headers=request_headers, params=params) as response:
            http_status_code = response.status
            response_body = await response.json()

            kucoin_system_code = int(response_body['code'])

            if http_status_code != 200 or kucoin_system_code != 200000:
                logger.error('Error sending GET Request: \n' +
                             f'{get_http_error_message(http_status_code)} ' +
                             f'{get_kucoin_system_error_message(kucoin_system_code)}')

            return response_body['data']

    async def post_request(self, path_ending: str, body_data: dict):

        if self.session is None:
            self.session = aiohttp.ClientSession()

        url = self.API_URL + path_ending

        # create the request headers
        request_headers = copy.deepcopy(self.headers_dict)
        request_headers['timeout'] = str(10)

        # sign the request
        # from the API docs
        now = int(time.time() * 1000)
        request_headers['KC-API-TIMESTAMP'] = str(now)
        request_headers['KC-API-SIGN'] = generate_rest_signature(now, 'POST', path_ending, body_data).decode('utf-8')

        async with self.session.post(url, headers=request_headers, data=body_data) as response:
            http_status_code = response.status
            response_body = await response.json()

            kucoin_system_code = int(response_body['code'])

            if http_status_code != 200 or kucoin_system_code != 200000:
                logger.error('Error sending POST Request: \n' +
                             f'{get_http_error_message(http_status_code)} ' +
                             f'{get_kucoin_system_error_message(kucoin_system_code)}')

            return response_body

    async def delete_request(self, path_ending: str, body_data: dict):

        if self.session is None:
            self.session = aiohttp.ClientSession()

        url = self.API_URL + path_ending

        # create the request headers
        request_headers = copy.deepcopy(self.headers_dict)
        request_headers['timeout'] = str(10)

        # sign the request
        # from the API docs
        now = int(time.time() * 1000)
        request_headers['KC-API-TIMESTAMP'] = str(now)
        request_headers['KC-API-SIGN'] = generate_rest_signature(now, 'DELETE', path_ending, body_data).decode('utf-8')

        async with self.session.post(url, headers=request_headers, data=body_data) as response:
            http_status_code = response.status
            response_body = await response.json()

            kucoin_system_code = int(response_body['code'])

            if http_status_code != 200 or kucoin_system_code != 200000:
                logger.error('Error sending DELETE Request: \n' +
                             f'{get_http_error_message(http_status_code)} ' +
                             f'{get_kucoin_system_error_message(kucoin_system_code)}')

            return response_body

    async def put_request(self, path_ending: str, body_data: dict):

        if self.session is None:
            self.session = aiohttp.ClientSession()

        url = self.API_URL + path_ending

        # create the request headers
        request_headers = copy.deepcopy(self.headers_dict)
        request_headers['timeout'] = str(10)

        # sign the request
        # from the API docs
        now = int(time.time() * 1000)
        request_headers['KC-API-TIMESTAMP'] = str(now)
        request_headers['KC-API-SIGN'] = generate_rest_signature(now, 'PUT', path_ending, body_data).decode('utf-8')

        async with self.session.post(url, headers=request_headers, data=body_data) as response:
            http_status_code = response.status
            response_body = await response.json()

            kucoin_system_code = int(response_body['code'])

            if http_status_code != 200 or kucoin_system_code != 200000:
                logger.error('Error sending PUT Request: \n' +
                             f'{get_http_error_message(http_status_code)} ' +
                             f'{get_kucoin_system_error_message(kucoin_system_code)}')

            return response_body

    async def get_timestamp(self):
        return await self.get_request('/api/v1/timestamp')

    async def get_status(self):
        return await self.get_request('/api/v1/status')

    async def get_currencies(self):
        return await self.get_request('/api/v1/currencies')

    async def get_currency(self, symbol):
        return await self.get_request(f'/api/v2/currencies/{symbol}')

    async def get_accounts(self):
        return await self.get_request('/api/v1/accounts')

    async def get_account(self, account_id):
        return await self.get_request(f'/api/v1/accounts/{account_id}')

    async def get_ticker(self, symbol: str) -> Union[TickerInfo, None]:

        data_dict = await self.get_request(f'/api/v1/market/orderbook/level1?symbol={symbol}')

        ticker = TickerInfo(
            sequence=int(data_dict['sequence']),
            best_ask=Decimal(data_dict['bestAsk']),
            size=Decimal(data_dict['size']),
            price=Decimal(data_dict['price']),
            best_bid=Decimal(data_dict['bestBid']),
            best_ask_size=Decimal(data_dict['bestAskSize']),
            best_bid_size=Decimal(data_dict['bestBidSize']),
            time=(data_dict['time'])
        )

        return ticker

    async def get_symbol_info(self, symbol: str) -> Union[SymbolInfo, None]:

        symbol_infos = await self.get_all_symbol_info()

        for s in symbol_infos:
            if s.symbol == symbol:
                return s

        return None

    async def get_all_symbol_info(self) -> List[SymbolInfo]:

        symbol_infos = []

        data_dict = await self.get_request(f'/api/v2/symbols')

        for s in data_dict:

            mf = s["minFunds"]
            if mf is None:
                mf = Decimal(0.000000001)

            symbol = SymbolInfo(
                symbol=s["symbol"],
                name=s["name"],
                base_currency=s["baseCurrency"],
                quote_currency=s["quoteCurrency"],
                fee_currency=s["feeCurrency"],
                market=s["market"],
                base_min_size=Decimal(s["baseMinSize"]),
                quote_min_size=Decimal(s["quoteMinSize"]),
                base_max_size=Decimal(s["baseMaxSize"]),
                quote_max_size=Decimal(s["quoteMaxSize"]),
                base_increment=Decimal(s["baseIncrement"]),
                quote_increment=Decimal(s["quoteIncrement"]),
                price_increment=Decimal(s["priceIncrement"]),
                price_limit_rate=Decimal(s["priceLimitRate"]),
                min_funds=Decimal(mf),
                is_margin_enabled=s["isMarginEnabled"],
                enable_trading=s["enableTrading"]
            )
            symbol_infos.append(symbol)
        return symbol_infos

    async def create_market_order(self):
        pass

    async def create_limit_order(self):
        pass

    async def cancel_order(self, order_id):
        return await self.delete_request(f'/api/v1/orders/{order_id}', {})

    # async def cancel_order_by_client_order_id(self):
    #     pass

    async def cancel_all_orders(self):
        return await self.delete_request('/api/v1/orders', {})

    async def get_orders(self):
        return await self.get_request('/api/v1/orders')

    async def get_order(self, order_id):
        return await self.get_request(f'/api/v1/orders/{order_id}')

    async def get_order_history(self):
        return await self.get_request('/api/v1/limit/orders')

    # async def get_order_by_client_order_id(self):
    #     pass

    async def get_order_book_snapshot(self, symbol: str, current_price: Decimal,
                                      price_increment: Decimal, depth: int = 100) -> Union[OrderBook, None]:

        data_dict = await self.get_request(f'/api/v1/market/orderbook/level2_{depth}?symbol={symbol}')

        sequence = int(data_dict['sequence'])
        _timestamp = int(data_dict['time'])

        order_book = OrderBook(current_price, price_increment)
        order_book.snapshot_update(sequence, data_dict['bids'], data_dict['asks'])

        return order_book

    async def get_full_order_book_snapshot(self):
        pass

    async def get_websocket_channel_info(self):
        response_body = await self.post_request('/api/v1/bullet-private', {})

        data_dict = response_body['data']
        instance_server = data_dict['instanceServers'][0]

        _kucoin_system_code = int(response_body['code'])
        ws_connect_token = data_dict['token']
        endpoint = instance_server['endpoint']
        encrypt = bool(instance_server['encrypt'])
        protocol = instance_server['protocol']
        ping_interval = int(instance_server['pingInterval'])
        ping_timeout = int(instance_server['pingTimeout'])

        ws_info = WebsocketInfo(
            ws_connect_token=ws_connect_token,
            endpoint=endpoint,
            encrypt=encrypt,
            protocol=protocol,
            ping_interval=ping_interval,
            ping_timeout=ping_timeout
        )

        return ws_info
