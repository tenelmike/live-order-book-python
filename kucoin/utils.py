
import time
from decimal import Decimal
from dataclasses import dataclass


@dataclass
class TickerInfo:
    """
    Dataclass for storing info about ticker
    """
    # sequence number
    sequence: int

    # best ask price
    best_ask: Decimal

    # last traded size
    size: Decimal

    # last traded price
    price: Decimal

    # best bid price
    best_bid: Decimal

    # best ask size
    best_ask_size: Decimal

    # best bid size
    best_bid_size: Decimal

    # timestamp
    time: int


@dataclass
class SymbolInfo:
    """
    Dataclass for storing info about trading symbols
    """
    # unique code of a symbol, will not change after renaming
    symbol: str

    # Name of trading pairs, will change after renaming
    name: str

    # Base currency, e.g. BTC
    base_currency: str

    # Quote currency, e.g. USDT.
    quote_currency: str

    # the currency of charged fees
    fee_currency: str

    # the trading market
    market: str

    # the minimum order quantity requried to place an order
    base_min_size: Decimal

    # the minimum order funds required to place a market order
    quote_min_size: Decimal

    # the maximum order size required to place an order
    base_max_size: Decimal

    # the maximum order funds required to place a market order
    quote_max_size: Decimal

    # the increment of the order size. The value shall
    # be a positive multiple of the baseIncrement
    base_increment: Decimal

    # the increment of the funds required to place a
    # market order. The value shall be a positive
    # multiple of the quoteIncrement
    quote_increment: Decimal

    # the increment of the price required to place a limit
    # order. The value shall be a positive multiple of the
    # priceIncrement
    price_increment: Decimal

    # threshold for price portection
    price_limit_rate: Decimal

    # the minimum spot and margin trading amounts
    min_funds: Decimal

    # available for margin or not
    is_margin_enabled: bool

    # available for transaction or not
    enable_trading: bool

    def __repr__(self):
        return f"SymbolInfo(symbol={self.symbol}, name={self.name}, " \
               f"base_currency={self.base_currency}, " \
               f"quote_currency={self.quote_currency}, " \
               f"fee_currency={self.fee_currency}, market={self.market}, " \
               f"base_min_size={self.base_min_size}, " \
               f"quote_min_size={self.quote_min_size}, " \
               f"base_max_size={self.base_max_size}, " \
               f"quote_max_size={self.quote_max_size}, " \
               f"base_increment={self.base_increment}, " \
               f"quote_increment={self.quote_increment}, " \
               f"price_increment={self.price_increment}, " \
               f"price_limit_rate={self.price_limit_rate}, " \
               f"min_funds={self.min_funds}, " \
               f"is_margin_enabled={self.is_margin_enabled}, " \
               f"enable_trading={self.enable_trading})"


@dataclass
class WebsocketInfo:
    """
    Dataclass for storing info about the websocket configuration
    """
    # token to connect the websocket session
    ws_connect_token: str

    # the URL to connect to
    endpoint: str

    # whether the websocket needs to use SSL
    encrypt: bool

    # the protocol to use - should always be 'websocket'
    protocol: str

    # the interval the client should send a ping to keep the connection open
    ping_interval: int

    # the extreme timeout value where the server will close the connection if no ping is received
    ping_timeout: int

    def __repr__(self):
        ws_str = f"WebSocket connection token: {self.ws_connect_token}\n"
        ws_str += f"Endpoint URL: {self.endpoint}\n"
        ws_str += f"Encryption required: {self.encrypt}\n"
        ws_str += f"Protocol: {self.protocol}\n"
        ws_str += f"Ping interval: {self.ping_interval} ms\n"
        ws_str += f"Ping timeout: {self.ping_timeout} ms\n"
        return ws_str


def make_kucoin_unique_id_str():
    """
    Gets the time in ns and converts to a string to get a monotonic unique id
    :return: id str
    """
    return str(make_kucoin_unique_id_int())


def make_kucoin_unique_id_int():
    """
    Gets the time in ns to get a monotonic unique id
    :return: id str
    """
    return int(time.time_ns() / 1000000)


def make_kucoin_websocket_url(ws_info: WebsocketInfo, private_ws=False):
    """
    Makes a connection url for opening the websocket

    :param ws_info: The info received from the REST call
    :param private_ws: Whether the Websocket will have private user info or public data
    :return: the URL for opening the websocket
    """
    # pick a connection id that will be used in the welcome and error messages
    connection_id = make_kucoin_unique_id_str()
    token = ws_info.ws_connect_token
    base_url = ws_info.endpoint

    ws_url = f'{base_url}?token={token}&connectId={connection_id}'

    if private_ws:
        ws_url += '&acceptUserMessage=true'

    return ws_url


def get_http_error_message(status_code: int):
    """
    Returns human readable error messages from http status codes

    :param status_code: the http status code
    :return: the human readable error
    """

    if status_code == 200:
        return "OK -- Success."
    elif status_code == 400:
        return "Bad Request -- Invalid request format."
    elif status_code == 401:
        return "Unauthorized -- Invalid API Key."
    elif status_code == 403:
        return "Forbidden or Too Many Requests -- The request is forbidden or Access limit breached."
    elif status_code == 404:
        return "Not Found -- The specified resource could not be found."
    elif status_code == 405:
        return "Method Not Allowed -- You tried to access the resource with an invalid method."
    elif status_code == 415:
        return "Unsupported Media Type. You need to use: application/json."
    elif status_code == 500:
        return "Internal Server Error -- We had a problem with our server. Try again later."
    elif status_code == 503:
        return "Service Unavailable -- We're temporarily offline for maintenance. Please try again later."
    else:
        return "Unknown HTTP error code."


def get_kucoin_system_error_message(error_code: int):
    """
    Returns human readable error messages from error codes

    :param error_code: the system error code
    :return: the human readable error
    """
    if error_code == 200000:
        return "Success (200000)."
    elif error_code == 200001:
        return "Order creation for this pair suspended (200001)."
    elif error_code == 200002:
        return "Order cancel for this pair suspended (200002)."
    elif error_code == 200003:
        return "Number of orders breached the limit (200003)."
    elif error_code == 200009:
        return "Please complete the KYC verification before you trade (200009)."
    elif error_code == 200004:
        return "Balance insufficient (200004)."
    elif error_code == 400001:
        return "Any of KC-API-KEY, KC-API-SIGN, KC-API-TIMESTAMP, " \
               "KC-API-PASSPHRASE is missing in your request header (400001)."
    elif error_code == 400002:
        return "KC-API-TIMESTAMP Invalid (400002)."
    elif error_code == 400003:
        return "KC-API-KEY not exists (400003)."
    elif error_code == 400004:
        return "KC-API-PASSPHRASE error (400004)."
    elif error_code == 400005:
        return "Signature error (400005)."
    elif error_code == 400006:
        return "The requested IP address is not in the API whitelist (400006)."
    elif error_code == 400007:
        return "Access Denied (400007)."
    elif error_code == 404000:
        return "URL Not Found (404000)."
    elif error_code == 400100:
        return "Parameter Error (400100)."
    elif error_code == 400200:
        return "Forbidden to place an order (400200)."
    elif error_code == 400500:
        return "Your located country/region is currently not supported for the trading of this token (400500)."
    elif error_code == 400700:
        return "Transaction restricted, there's a risk problem in your account (400700)."
    elif error_code == 400800:
        return "Leverage order failed (400800)."
    elif error_code == 411100:
        return "User is frozen (411100)."
    elif error_code == 415000:
        return "Unsupported Media Type -- The Content-Type of the request header " \
               "needs to be set to application/json (415000)."
    elif error_code == 500000:
        return "Internal Server Error (500000)."
    elif error_code == 900001:
        return "Symbol not exists (900001)."
    else:
        return f"Unknown KuCoin error code ({error_code})."
