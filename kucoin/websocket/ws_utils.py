
from decimal import Decimal
import logging
import asyncio


# Create a logger object
logger = logging.getLogger('kucoin.websocket')

# Set the log level to DEBUG so that all messages are logged
logger.setLevel(logging.DEBUG)


async def message_queue_callback(event, queue: asyncio.Queue):
    """
    Callback that adds an event to the message queue when it is received (non-blocking)

    :param event: The event to add to the queue
    :param queue: The queue we are adding the event to
    """
    if queue is not None:
        queue.put_nowait(event)


async def parse_order_book_update(order_msg, websocket):
    """
    Parses an order book update message received on the websocket

    :param order_msg: The message received as a json dict
    :param websocket: The websocket that received the message
    """
    order_data = order_msg['data']

    sequence_start = order_data['sequenceStart']
    sequence_end = order_data['sequenceEnd']

    # If the sequence numbers are newer than the order book snapshot, apply the update to the book
    if sequence_start <= (websocket.order_book.sequence_number + 1) and (
            sequence_end > websocket.order_book.sequence_number):

        # get the bid and ask updates
        bid_update = order_data['changes']['bids']
        ask_update = order_data['changes']['asks']

        for bid in bid_update:
            websocket.order_book.bids.update_amount_at_price_level(Decimal(bid[0]), Decimal(bid[1]))

        for ask in ask_update:
            websocket.order_book.asks.update_amount_at_price_level(Decimal(ask[0]), Decimal(ask[1]))

        # update the sequence number for the next message
        websocket.order_book.sequence_number = sequence_end


async def parse_match_message(match_msg, websocket):
    """
    Parses a match message received on the websocket

    :param match_msg: The match message received
    :param websocket: The websocket that received the message
    """
    match_data = match_msg['data']

    side = match_data['side']
    price = match_data['price']
    quantity = match_data['size']
    trade_id = match_data['tradeId']
    taker_id = match_data['takerOrderId']
    maker_id = match_data['makerOrderId']
    timestamp_ns = int(match_data['time'])

    # record the match in the log of trades that were filled from the order book
    websocket.trade_fills.create_match_fill(timestamp_ns, side, price, quantity, trade_id, maker_id, taker_id)


async def message_processing_coroutine(websocket):
    """
    The coroutine that waits for messages in the queue and processes them depending on what kind of message it is

    :param websocket: The websocket receiving the messages
    """
    # Coroutine runs as long as the websocket is connected
    while websocket.keep_running:

        # Blocks until a message is received
        try:
            socket_message = await asyncio.wait_for(websocket.message_queue.get(), timeout=1.0)
        except asyncio.TimeoutError:
            continue

        # Parse the message depending on the type
        if '/market/match' in str(socket_message['topic']):
            await parse_match_message(socket_message, websocket)
        elif '/market/level2' in str(socket_message['topic']):
            await parse_order_book_update(socket_message, websocket)

    logger.info('Message Processing Coroutine Terminated')
